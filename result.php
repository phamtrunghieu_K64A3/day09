<?php
    session_start();

    define("true_answer", [
        "answer_1" => "c",
        "answer_2" => "a",
        "answer_3" => "c",
        "answer_4" => "b",
        "answer_5" => "a",
        "answer_6" => "d",
        "answer_7" => "c",
        "answer_8" => "d",
        "answer_9" => "c",
        "answer_10" => "b",
    ]);

    $count = 0;
    $empty = 0;

    foreach (true_answer as $key => $answer) {
        if ($_COOKIE[$key] == 'e') {
            $empty++;
        }
        if ($answer == $_COOKIE[$key])
            $count++;
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/result.css">
</head>
<body>
    <div class="wrapper">
        <?php
        if ($empty != 0) {
            echo "Bạn làm đúng " . $count . " câu " . "(" . $empty . " câu bị bỏ trống)" ."</br>";
        }
        else {
            echo "Bạn làm đúng " . $count . " câu " ."</br>";
        }
        if ($count < 4) {
            echo "Bạn quá kém, cần ôn tập thêm";
        }
        elseif ($count < 7) {
            echo "Cũng bình thường";
        }
        else {
            echo "Sắp sửa làm được trợ giảng lớp PHP";
        }
        ?>

    </div>
</body>
</html>