<?php
    session_start();

    $questions = [
        "Con nào sau đây có bốn chân?" => ["Gà", "Rắn", "Vịt", "Mèo"],
        "Trái đất hình gì?" => ["bầu dục", "hộp chữ nhật", "cầu", "tam giác"],
        "Con gì biết phát sáng?" => ["Con ruồi", "Con mèo", "Con chó", "Con đom đóm"],
        "Fan Sơn tùng gọi là gì?" => ["Đóm", "Army", "Sky", "Gia đình Villa"],
        "Đạt villa gọi fan của mình là gì?" => ["Đóm", "Gia đình Villa", "Sky", "Army"]
    ];
    $answer_labels = ["a", "b", "c", "d"];

    if (isset($_POST['submit'])) {
        for ($i = 6; $i <= 10; $i++) {
            if (isset($_POST['answer_' . $i])) {
                $_SESSION['answer_' . $i] = $_POST['answer_' . $i];
                setcookie('answer_' . $i,  $_POST['answer_' . $i], time() + (86400 * 30), "/");
            }
            else {
                $_SESSION['answer_' . $i] = "e";
                setcookie('answer_' . $i,  "e", time() + (86400 * 30), "/");
            }
        }
        header('Location: '.'result.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div class="wrapper">
        <div class="title-field">Câu hỏi trắc nghiệm thú vị</div>
        <form action="", method="post">
            <?php
            $count = 6;
            foreach ($questions as $question => $answers) {
            ?>
                <div class="question-box">
                    <div class="question-filed"><?php echo "Câu " . $count . ": " . $question ?></div>
                    <?php
                    for ($i = 0; $i < sizeof($answers); $i++) {
                    ?>
                        <div class="answer-field">
                            <input type="radio" name=<?php echo "answer_" . $count ?> value=<?php echo $answer_labels[$i] ?> id=<?php echo "answer_" . $count . $answer_labels[$i]; ?>>
                            <label for=<?php echo "answer_" . $count . $answer_labels[$i]; ?>><?php echo $answers[$i] ?></label>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            <?php
            $count++;
            }
            ?>
            <button name="submit" id="button-submit" class="button-submit" type="submit">Nộp bài</button>
        </form>  
    </div>
</body>
</html>