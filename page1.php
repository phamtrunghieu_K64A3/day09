<?php
    session_start();

    $questions = [
        "Tại sao lá cây lại màu xanh?" => ["Trời cho", "Màu này đẹp", "Chất diệp lục", "Lá cây màu đỏ mà"],
        "Hoa đào nở vào mùa?" => ["Mùa xuân", "Mùa hè", "Mùa thu", "Mùa đông"],
        "Quả táo có vị gì?" => ["Cay", "Đắng", "Ngọt", "Bùi"],
        "Cây cho ra củ là?" => ["Cây mít", "Khoai lang", "Cây táo", "Hoa hồng"],
        "Con thỏ thích ăn gì?" => ["Cà rốt", "Cà tím", "Chuối", "Gạo"]
    ];
    $answer_labels = ["a", "b", "c", "d"];

    if (isset($_POST['submit'])) {
        for ($i = 1; $i <= 5; $i++) {
            if (isset($_POST['answer_' . $i])) {
                $_SESSION['answer_' . $i] = $_POST['answer_' . $i];
                setcookie('answer_' . $i,  $_POST['answer_' . $i], time() + (86400 * 30), "/");
            }
            else {
                $_SESSION['answer_' . $i] = "e";
                setcookie('answer_' . $i,  "e", time() + (86400 * 30), "/");
            }
        }
        header('Location: '.'page2.php');
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div class="wrapper">
        <div class="title-field">Câu hỏi trắc nghiệm thú vị</div>
        <form action="" method="post">
            <?php
            $count = 1;
            foreach ($questions as $question => $answers) {
            ?>
                <div class="question-box">
                    <div class="question-filed"><?php echo "Câu " . $count . ": " . $question ?></div>
                    <?php
                    for ($i = 0; $i < sizeof($answers); $i++) {
                    ?>
                        <div class="answer-field">
                            <input type="radio" name=<?php echo "answer_" . $count ?> value=<?php echo $answer_labels[$i] ?>  id=<?php echo "answer_" . $count . $answer_labels[$i]; ?>>
                            <label for=<?php echo "answer_" . $count . $answer_labels[$i]; ?>><?php echo $answers[$i] ?></label>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            <?php
            $count++;
            }
            ?>
            <button name="submit" id="button-submit" class="button-submit" type="submit">Next</button>
        </form>  
    </div>
</body>
</html>